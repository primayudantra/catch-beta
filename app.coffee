###=======================
- This Code for CATCH
- Author by @PrimaYudantra
- Version :  1.0
- Date : November 29, 2015
======================= ###

### ***************************
# 		Init Library
#  Made by @PrimaYudantra
# *************************** ###
express 		= require 'express'
http 			= require 'http'
async			= require 'async'
bodyParser 		= require 'body-parser'
path 			= require 'path'
fse 			= require 'fs-extra'
Pipa			= require 'pipa'
mongojs 		= require 'mongojs'
passport 		= require 'passport'
LocalStrategy 	= require 'passport-local'
moment			= require 'moment'
app 			= express()
cookieParser 	= require 'cookie-parser'
cookieSession 	= require 'cookie-session'
expressSession 	= require 'express-session'
RedisStore		= require('connect-redis') expressSession

### ***************************
# 		EXPRESS SETUP
# *************************** ###
ejs = require('ais-ejs-mate')({ open: '{{', close:'}}'})

### ***************************
#		DATABASE SETUP
# *************************** ###
db = mongojs 'catchdb', ['user','admin','dream']

### ***************************
# 		SETUP PASSPORT
# *************************** ###
passport.use new LocalStrategy({
		usernameField : 'username'
		passwordField : 'password'
		},
		(username, password, done) ->
			db.user.findOne {username : username}, (err, user) ->
				if err
					return done err
				if username is null and password is null
					console.log "User not found 1"
				if not username
					console.log "User not found 2" 
				if not password
					console.log "Password not found"
				else
					return done null, user
	)

### ***************************
#		MOMENT SETUP
s
# *************************** ###
moment().format()

### ***************************
# 		EXPRESS SETUP
# *************************** ###
app.engine '.html', ejs
app.use bodyParser()
app.use bodyParser.urlencoded({extended: false})
app.use bodyParser.json()

app.set 'views', __dirname + '/views' 
app.use(express.static(__dirname + '/public'));
app.set 'view engine', 'html'

sessionStoreOpts = new RedisStore
	db : 1
	prefix: 'sess:'

expressSessionOpts =
	store: sessionStoreOpts
	secret: 'test'
	saveUnintialized: true
	resave: true
	cookie:
		secure: false
		maxAge: 60 * 60 * 1000

app.use expressSession expressSessionOpts
app.use passport.initialize()
app.use passport.session()

passport.serializeUser (user, done) ->
	console.log "serializeUser"
	done null, user

passport.deserializeUser (user, done) ->
	console.log "deserializeUser"
	db.user.findOne {username : user.username}, (err, user) ->
		done err, user

### ***************************
# 		PORT SETUP
# *************************** ###
port = 8989

### ***************************
# 		PIPAJS SETUP
#	Thanks to @FarishJazz for this Framework =D
# *************************** ###
pipa = new Pipa app,'router','controller'
pipa.open()

### ***************************
# 		SERVER RUNNING ON
# *************************** ###
app.listen port, ()->
	console.log ""
	console.log "==============================="
	console.log "Welcome to the CATCH"
	console.log "Server running on localhost:" + port
	console.log "==============================="

	