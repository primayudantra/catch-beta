mongojs 	= require 'mongojs'
moment		= require 'moment'
Pipa		= require 'pipa'
passport 	= require 'passport'
db 			= mongojs 'catchdb', ['user', 'dream']

module.exports = 
	dreamList: (req,res) ->
		data = {}
		db.dream.find (err, result) ->
			data =
				dataDream : result
			data.user = req.user
			res.render "dream", data
	dreamTimeline: (req,res) ->
		data = {}
		data.user = req.user
		res.render "timeline-dream", data
	postDream: (req,res,next) ->
		data =
			dreamContent 	: req.body.dreamContent
			dreamTopic		: req.body.dreamTopic
			created_at 		: new Date
			user 			: req.user._id
		db.dream.save data, (err, result) ->
			if err
				console.dir err
			else
				console.log "Dream Inserted"
				next()
		console.log data
		res.redirect 'home'
