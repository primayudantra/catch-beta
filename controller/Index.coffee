mongojs 	= require 'mongojs'
Pipa		= require 'pipa'
passport 	= require 'passport'
db 			= mongojs 'catchdb', ['user']
module.exports = 
	### ***************************
	# 			LOGIN
	#  Made by @PrimaYudantra
	#  Method : Get
	#  Title : Get Registration Page
	# *************************** ###
	loginIndex: (req,res,next) ->
		res.render 'login' 

	postLoginIndex: (req, res, next) ->
	 	passport.authenticate('local', (err, user) ->
	 		if err
	 			return next(err)
	 		req.login user, (err) ->
	 			if err
	 				res.send "error for Authentication"
	 			res.redirect '/home'
	 	)(req,res,next)
		

	testAuth: (req,res) ->
		console.log "testAuth"
		# return res.redirect '/' if not req.user
		res.json { message: 'success', user: req.user}

	### ***************************
	# 			LOGOUT
	#  Made by @PrimaYudantra
	#  Method : Get
	#  Title : Logout Function
	# *************************** ###
	logout: (req,res,next) ->
		req.logout()
		res.redirect '/'

	### ***************************
	# 			REGISTRATION
	#  Made by @PrimaYudantra
	#  Method : Get
	#  Title : Get Registration Page
	# *************************** ###
	registerIndex: (req, res, next)->
		res.render "register"
	postRegisterIndex: (req,res,next)->
		data =
			username 	: req.body.username_reg
			password 	: req.body.password_reg
			email 		: req.body.email_reg
		console.log data
		db.user.save data, (err, result) ->
			if err
				console.dir err
			else
				console.log "DB Inserted"
				next()
		res.redirect '/'

	### ***************************
	#  			INDEX
	# *************************** ###
	homeIndex:	(req,res,next) ->
		data = {}
		data.user = req.user
		res.render 'index', data
