mongojs 	= require 'mongojs'
Pipa		= require 'pipa'
passport 	= require 'passport'
db 			= mongojs 'catchdb', ['user']
module.exports=
	profileIndex: (req,res,next)->
		data = {}
		data.user = req.user
		res.render "profile", data
	editProfile: (req,res,next) ->
		id = req.params.id
		data = {}
		data.user = req.user
		res.render "edit-profile", data
	postEditProfile: (req,res,next) ->
		id = req.body.userID
		data =
			password 	: req.body.password
			fullname 	: req.body.fullname
			bio			: req.body.bio
			dob			: req.body.dob
			topic		: req.body.topic
		data.user = req.user
		console.dir data
		console.log id
		db.user.update {_id:mongojs.ObjectId(id)}, {$set: data}, {upsert: true}, (err, result) ->
			if err
				console.dir err
			else
				console.log "Updated for ID : _" + id
			res.redirect 'profile'
		# res.redirect '/edit-profile-:id'